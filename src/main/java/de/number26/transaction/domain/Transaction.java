package de.number26.transaction.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Represents a single transaction
 */
public class Transaction implements Serializable {

    private static final long serialVersionUID = 5234237773434l;

    @JsonIgnore
    private final long id;
    private final Long parentId;
    @NotNull
    private final String type;
    private final double amount;

    public Transaction(long id, Long parentId, String type, double amount) {
        this.id = id;
        this.type = type;
        this.parentId = parentId;
        this.amount = amount;
    }

    public long getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public Long getParentId() {
        return parentId;
    }

    public double getAmount() {
        return amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Transaction that = (Transaction) o;

        if (id != that.id) return false;
        if (Double.compare(that.amount, amount) != 0) return false;
        if (parentId != null ? !parentId.equals(that.parentId) : that.parentId != null) return false;
        return !(type != null ? !type.equals(that.type) : that.type != null);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = (int) (id ^ (id >>> 32));
        result = 31 * result + (parentId != null ? parentId.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        temp = Double.doubleToLongBits(amount);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", parentId=" + parentId +
                ", type='" + type + '\'' +
                ", amount=" + amount +
                '}';
    }
}
