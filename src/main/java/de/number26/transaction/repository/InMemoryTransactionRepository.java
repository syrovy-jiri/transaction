package de.number26.transaction.repository;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.SetMultimap;
import de.number26.transaction.domain.Transaction;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.*;

/**
 * Almost all methods here give us O(1) complexity except for sumOfTransactions()
 * See below
 */
@Repository
public class InMemoryTransactionRepository implements TransactionRepository {

    /*
     * Map to store all transactions by id
     */
    private Map<Long, Transaction> transactionsById = new HashMap<>();
    private SetMultimap<String, Long> transactionsByType = HashMultimap.create();
    /*
     * Reverse multimap of parentId -> descendantIds
     */
    private Multimap<Long, Long> linkedTransactionIds = HashMultimap.create();

    @Override
    public void saveTransaction( Transaction transaction ) throws EntityExistsException, EntityNotFoundException {
        // check if the transaction is not already associated with the given id
        if ( transactionsById.putIfAbsent( transaction.getId(), transaction ) != null ) {
            throw new EntityExistsException(String.format("Transaction already exists %d", transaction.getId()));
        }
        // only map if the type is specified
        if ( transaction.getType() != null ) {
            transactionsByType.put( transaction.getType(), transaction.getId() );
        }
        // update descendant map
        if ( transaction.getParentId() != null ) {
            if ( ! transactionsById.containsKey( transaction.getParentId() ) ) {
                throw new EntityNotFoundException("Parent transaction not found");
            }
            linkedTransactionIds.put(transaction.getParentId(), transaction.getId());
        }
    }

    @Override
    public Transaction findById( Long id ) throws EntityNotFoundException {
        Transaction transaction = transactionsById.get(id);
        if ( transaction == null ) {
            throw new EntityNotFoundException();
        }
        return transaction;
    }

    @Override
    public Set<Long> findByType( String type ) {
        return transactionsByType.get( type );
    }

    @Override
    public boolean transactionExists( Long id ) {
        return transactionsById.containsKey(id);
    }

    /**
     * Calculates transitive sum of all transactions linked to the current transaction.
     * Uses queue to avoid having too deep stack and potential <code>StackOverflowException</code>
     * Time complexity of the calculation is O(n) where n is the number of linked transactions to the current one
     * or O(m*n) where m is an average number of descendants and n an average depth of transaction.
     *
     * @param id transaction id to start with
     * @return sum of all transaction transitively linked to the current transaction using parentId
     */
    @Override
    public double sumOfTransactions( Long id ) throws EntityNotFoundException {

        double sum = 0.0;
        // use queue to prevent StackOverflowException
        Deque<Long> toBeProcessed = new ArrayDeque<>( );
        while ( id != null ) {
            Transaction currentTransaction = findById( id );
            sum += currentTransaction.getAmount();
            Collection<Long> childrenIds = linkedTransactionIds.get( id );
            if ( childrenIds != null ) {
                toBeProcessed.addAll( childrenIds );
            }
            id = toBeProcessed.pollFirst();
        }
        return sum;
    }

}
