package de.number26.transaction.repository;

import de.number26.transaction.domain.Transaction;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.Set;

public interface TransactionRepository {
    /**
     * Creates or overwrites existing <code>Transaction</code>
     * in the repository
     *
     * @param transaction is the <code>Transaction</code> to be saved
     * @throws EntityExistsException if the transaction is already associated with the given transaction id
     */
    void saveTransaction( Transaction transaction ) throws EntityExistsException, EntityNotFoundException;

    /**
     * Returns a single <code>Transaction</code identified by a given id
     *
     * @param id is the id of the <code>Transaction</code>
     * @return found transaction
     * @throws EntityNotFoundException if no <code>transaction</code> is found
     */
    Transaction findById( Long id ) throws EntityNotFoundException;

    /**
     * Find all transactions with a given type
     *
     * @param type of the <code>Transaction</code> to be found
     * @return a set of ids of <code>Transaction</code> of a given type
     */
    Set<Long> findByType( String type );

    /**
     * Checks if the <code>Transaction</code> exists
     *
     * @return true if <code>Transaction</code> exists
     */
    boolean transactionExists( Long id );

    /**
     * Sums the chain of transactions linked together by parentId attribute
     *
     * @param id of the first <code>Transaction</code> in the link
     * @return sum of all transactions linked together
     */
    double sumOfTransactions( Long id );
}

