package de.number26.transaction.web;

import de.number26.transaction.domain.Transaction;
import de.number26.transaction.repository.TransactionRepository;
import de.number26.transaction.web.dto.StatusDTO;
import de.number26.transaction.web.dto.SumDTO;
import de.number26.transaction.web.dto.TransactionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.Set;

@RestController
@RequestMapping("/transactionservice")
public class TransactionController {

    @Autowired
    private TransactionRepository transactionRepository;

    @RequestMapping(value = "/transaction/{transactionId}",
                    method = RequestMethod.PUT,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<StatusDTO> createTransaction(
           @PathVariable("transactionId") Long transactionId,
           @RequestBody TransactionDTO transaction ) {
        // just use simple validation in this case
        if ( transaction.getAmount() == null ) {
            return new ResponseEntity<StatusDTO>( StatusDTO.error("Amount is required"), HttpStatus.BAD_REQUEST );
        }
        if ( transaction.getType() == null ) {
            return new ResponseEntity<StatusDTO>( StatusDTO.error("Type is required"), HttpStatus.BAD_REQUEST );
        }
        transactionRepository.saveTransaction( transaction.toTransaction(transactionId) );
        return new ResponseEntity<StatusDTO>(StatusDTO.success(), HttpStatus.OK);
    }

    @RequestMapping(value = "/transaction/{transactionId}",
                    method = RequestMethod.GET,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TransactionDTO> fetchTransactionById(
            @PathVariable("transactionId") Long transactionId ) throws EntityNotFoundException {
        Transaction transaction = transactionRepository.findById( transactionId );
        return new ResponseEntity<TransactionDTO>(TransactionDTO.fromTransaction(transaction), HttpStatus.OK);
    }

    @RequestMapping(value = "/types/{type}",
                    method = RequestMethod.GET,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Set<Long>> fetchTransactionsByType(
            @PathVariable("type") String type ) {
        Set<Long> transactionIds = transactionRepository.findByType(type);
        return new ResponseEntity<Set<Long>>(transactionIds, HttpStatus.OK);
    }

     @RequestMapping(value = "/sum/{transactionId}",
                    method = RequestMethod.GET,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SumDTO> sumOfTransactions(
            @PathVariable("transactionId") Long transactionId ) throws EntityNotFoundException {
         double transactionSum = transactionRepository.sumOfTransactions(transactionId);
         return new ResponseEntity<SumDTO>(new SumDTO( transactionSum ), HttpStatus.OK);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<StatusDTO> notFound() {
        return new ResponseEntity<StatusDTO>(StatusDTO.error("Transaction not found"), HttpStatus.NOT_FOUND);
    }

    /*
     * Assumption is that the <code>Transaction</code> is immutable and once created it shouldn't be modified.
     */
    @ExceptionHandler(EntityExistsException.class)
    public ResponseEntity<StatusDTO> alreadyExists() {
        return new ResponseEntity<StatusDTO>(StatusDTO.error("Transaction already exists"), HttpStatus.CONFLICT);
    }

}
