package de.number26.transaction.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

/**
 * General purpose Status DTO in <code>TransactionController</code>
 */
public class StatusDTO {

    public static final String STATUS_OK = "ok";
    public static final String STATUS_ERROR = "error";

    public StatusDTO() { }
    public StatusDTO(String status, String message) {
        this.status = status;
        this.message = message;
    }

    @NotNull
    private String status;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Null
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static StatusDTO error(String message) {
        return new StatusDTO(STATUS_ERROR, message);
    }

    public static StatusDTO success() {
        return new StatusDTO(STATUS_OK, null);
    }
}
