package de.number26.transaction.web.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import de.number26.transaction.web.util.AmountSerializer;

/**
 * DTO for sum call in <code>TransactionController</code>
 */
public class SumDTO {

    @JsonSerialize(using = AmountSerializer.class)
    private Double sum;

    public SumDTO() {}

    public SumDTO( Double sum ) {
        this.sum = sum;
    }

    public Double getSum() {
        return sum;
    }

}
