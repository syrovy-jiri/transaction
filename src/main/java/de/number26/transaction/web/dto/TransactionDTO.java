package de.number26.transaction.web.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import de.number26.transaction.domain.Transaction;
import de.number26.transaction.web.util.AmountSerializer;

/**
 * General purpose transaction DTO
 */
public class TransactionDTO {
    private Long id;
    @JsonSerialize(using = AmountSerializer.class)
    private Double amount;
    private String type;
    private Long parentId;

    public TransactionDTO() {}

    public TransactionDTO(Long id, Double amount, String type, Long parentId) {
        this.id = id;
        this.amount = amount;
        this.type = type;
        this.parentId = parentId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Transaction toTransaction(long transactionId) {
        return new Transaction(transactionId, parentId, type, amount);
    }

    public static TransactionDTO fromTransaction(Transaction transaction) {
        return new TransactionDTO(
                    transaction.getId(),
                    transaction.getAmount(),
                    transaction.getType(),
                    transaction.getParentId());
    }

}
