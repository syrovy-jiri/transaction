package de.number26.transaction.web.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.math.BigDecimal;

/**
 * Provides "nice" serialization for double
 */
public class AmountSerializer extends JsonSerializer<Double> {
    @Override
    public void serialize(Double value, JsonGenerator generator, SerializerProvider provider) throws IOException {
        BigDecimal decimalValue = new BigDecimal(value);
        generator.writeNumber(decimalValue);
    }
}
