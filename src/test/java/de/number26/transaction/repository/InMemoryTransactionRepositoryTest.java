package de.number26.transaction.repository;

import de.number26.transaction.domain.Transaction;
import junit.framework.Assert;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;


public class InMemoryTransactionRepositoryTest {

    // operations with double are not precise. Use delta for comparison
    private static final double DELTA = 1e-10;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private TransactionRepository transactionRepository;

    @Before
    public void doBeforeEachTest() throws Exception {
        transactionRepository = new InMemoryTransactionRepository();
    }

    @Test
    public void transactionIsProperlyInserted() {
        long transactionId = RandomUtils.nextLong();
        String type = RandomStringUtils.random(10);
        double amount = RandomUtils.nextDouble();
        Transaction transaction = new Transaction(transactionId, null, type, amount);

        transactionRepository.saveTransaction(transaction);
        Assert.assertTrue(transactionRepository.transactionExists(transactionId));

        Transaction anotherTransaction = transactionRepository.findById(transactionId);
        Assert.assertEquals( transactionId, anotherTransaction.getId() );
        Assert.assertEquals( type, anotherTransaction.getType() );
        Assert.assertEquals(amount, anotherTransaction.getAmount());

    }

    @Test
    public void inconsistentTransactionThrowsException() {

        expectedException.expect(EntityNotFoundException.class);

        long transactionId = RandomUtils.nextLong();
        String type = RandomStringUtils.random(10);
        double amount = RandomUtils.nextDouble();
        Transaction transaction = new Transaction(transactionId, RandomUtils.nextLong(), type, amount);

        transactionRepository.saveTransaction(transaction);

    }

    @Test
    public void doubleInsertThrowsException() {
        long transactionId = RandomUtils.nextLong();
        String type = RandomStringUtils.random(10);
        double amount = RandomUtils.nextDouble();
        Transaction transaction = new Transaction(transactionId, null, type, amount);

        transactionRepository.saveTransaction(transaction);

        expectedException.expect(EntityExistsException.class);
        transactionRepository.saveTransaction(transaction);

    }

    @Test
    public void sumIsCorrectlyCalculatedAll() {
        String type = RandomStringUtils.random(10);

        Long previousId = null;
        Transaction transaction = null;
        // chain transactions together
        for (int count = 0; count < RandomUtils.nextInt(50); count++ ) {
            long transactionId = RandomUtils.nextLong();
            double amount = RandomUtils.nextDouble();
            // link all transactions together
            transaction = new Transaction(transactionId, previousId, type, amount);
            transactionRepository.saveTransaction(transaction);
            previousId = transactionId;
        }
        double sum = 0.0;
        // verify that sums are correct
        while ( ( previousId = transaction.getParentId() ) != null ) {
            sum += transaction.getAmount();
            double subSum = transactionRepository.sumOfTransactions( transaction.getId() );
            Assert.assertEquals(sum, subSum, DELTA);
            transaction = transactionRepository.findById( previousId );
        }
    }

    @Test
    public void sumIsCorrectlyCalculatedExample() {
        Transaction firstTransaction = new Transaction( 10l, null, "cars", 5000.0 );
        transactionRepository.saveTransaction(firstTransaction);
        Transaction secondTransaction = new Transaction( 11l, 10l, "shopping", 10000.0 );
        transactionRepository.saveTransaction(secondTransaction);
        Assert.assertEquals( 15000.0, transactionRepository.sumOfTransactions( 10l ));
    }

    @Test
    public void typeIsRetrievedCorrectly() {
        String typeA = RandomStringUtils.random(5);
        String typeB = RandomStringUtils.random(5);
        for ( long idx = 1; idx < 50; idx ++ ) {
            Transaction randomTransaction = new Transaction(idx, null, idx % 2 == 0 ? typeA : typeB , RandomUtils.nextDouble() );
            transactionRepository.saveTransaction(randomTransaction);
        }
        Assert.assertTrue( transactionRepository.findByType(typeA).stream().allMatch( t -> t % 2 == 0) );

        Assert.assertTrue( transactionRepository.findByType(typeB).stream().allMatch( t -> t % 2 == 1) );

    }

}
