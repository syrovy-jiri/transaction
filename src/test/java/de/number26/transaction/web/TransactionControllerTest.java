package de.number26.transaction.web;

import de.number26.transaction.repository.TransactionRepository;
import de.number26.transaction.web.dto.StatusDTO;
import de.number26.transaction.web.dto.SumDTO;
import de.number26.transaction.web.dto.TransactionDTO;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.persistence.EntityExistsException;

public class TransactionControllerTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @InjectMocks
    private TransactionController transactionController = new TransactionController();

    @Mock
    private TransactionRepository transactionRepository;


    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void transactionIsPutIntoRepository() throws Exception {
        Long testId = RandomUtils.nextLong();
        double amount = RandomUtils.nextDouble();
        String type = RandomStringUtils.random(10);
        TransactionDTO dto = new TransactionDTO(null, amount, type, null);

        when(transactionRepository.findById(testId)).thenReturn(null);
        ResponseEntity<StatusDTO> response = transactionController.createTransaction(testId, dto);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        Assert.assertEquals(response.getBody().getStatus(), StatusDTO.STATUS_OK);
    }

    @Test
    public void transactionSumSentCorrectly() throws Exception {

        Long testId = RandomUtils.nextLong();
        Double amount = RandomUtils.nextDouble();
        String type = RandomStringUtils.random(10);
        TransactionDTO dto = new TransactionDTO(null, amount, type, null);

        when(transactionRepository.sumOfTransactions(testId)).thenReturn( amount );

        ResponseEntity<SumDTO> response = transactionController.sumOfTransactions( testId );

        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        Assert.assertEquals(response.getBody().getSum(), amount);
    }

}
